from lxml import etree
from urllib.parse import unquote
import csv

# parse RDF file and create XML tree
tree = etree.parse('http://data.ifpri.org/lod/ghi.rdf')
root = tree.getroot()

content = root.findall('owl:Thing', root.nsmap)

# write header row to CSV file
header_row = ['country', 'year', 'index', 'severity']
with open('./data/ghi_data.csv', 'w') as csv_file:
    writer = csv.writer(csv_file, delimiter=',', lineterminator='\n')
    writer.writerow(header_row)
    
# loop through content and extract needed data from the lines
for row in content:
    
    if 'Global_Hunger_Index' in row.get('{http://www.w3.org/1999/02/22-rdf-syntax-ns#}about'):
        
        # extract, transform, clean and write data to CSV file
        with open('./data/ghi_data.csv', 'a') as csv_file:
            writer = csv.writer(csv_file, delimiter=',' , lineterminator='\n')
            
            c1 = unquote(row.find('{http://data.ifpri.org/lod/ghi/resource/}hasSpatialCoverage')                         .get('{http://www.w3.org/1999/02/22-rdf-syntax-ns#}resource'))                         .replace('http://aims.fao.org/aos/geopolitical.owl#','')                         .replace('__the','')                         .replace('_the','')            
            try:
                c2 = row.find('{http://data.ifpri.org/lod/ghi/resource/}hasYear').text
            except:
                c2 = ''
            try:
                c3 = row.find('{http://data.ifpri.org/lod/ghi/resource/}hasIndex').text
            except:
                c3 = ''
            try:
                c4 = row.find('{http://data.ifpri.org/lod/ghi/resource/}hasSeverity').text
            except:
                c4 = ''
            l = [c1, c2, c3, c4]
            writer.writerow(l)