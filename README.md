# Global Hunger Index

## Global Hunger Index for all World countries from 1992

### Definition:

The Global Hunger Index (GHI) is a tool designed to comprehensively measure and track hunger at global, regional, and national levels. GHI scores are calculated each year to assess progress and setbacks in combating hunger. The GHI is designed to raise awareness and understanding of the struggle against hunger, provide a way to compare levels of hunger between countries and regions, and call attention to those areas of the world where hunger levels are highest and where the need for additional efforts to eliminate hunger is greatest.
Measuring hunger is complicated. To use the GHI information most effectively, it helps to understand how the GHI scores are calculated and what they can and cannot tell us.
GHI is calculated over a period of several (4-6) years and is valid during that period.
* How are the GHI scores calculated?
GHI scores are calculated using a three-step process that draws on available data from various sources to capture the multidimensional nature of hunger.
First, for each country, values are determined for four indicators:
   - UNDERNOURISHMENT: the share of the population that is undernourished (that is, whose caloric intake is insufficient);
   - CHILD WASTING: the share of children under the age of five who are wasted (that is, who have low weight for their height, reflecting acute undernutrition);
   - CHILD STUNTING: the share of children under the age of five who are stunted (that is, who have low height for their age, reflecting chronic undernutrition); and
   - CHILD MORTALITY: the mortality rate of children under the age of five (in part, a reflection of the fatal mix of inadequate nutrition and unhealthy environments).
![](ghi.png)

## Data

Data obtained from IFPRI - International Food Policy Research Institute http://data.ifpri.org
Data source belongs to the organization which collect data, then it is primary data source
There are no missing values in the dataset and it is very consistent


## Preparation

Data is obtained using ghi.py script from the aforementioned web address. Script extract data from RDF format using lxml.
Basically it is XML structured data and script transforms and cleans data into CSV file ghi_data.csv.


## Usage

* Just clone or copy this repo on your local computer.
* Change path to folder where requirements.txt is located.
* To install requirements type or copy in your terminal: pip install -r requirements.txt
* Run ghi.py.


## Files

* /data/ghi_data.csv        - complete dataset for all Countries from 1997
* datapackage.json          - dataset metadata
* ghi.py                    - Python script that obtain and process whole dataset
* requirements.txt          - requirements
* README.md                 - this readme file


## License

Creative Commons Attribution-NonCommercial 4.0 (CC-BY-NC-4.0)
https://creativecommons.org/licenses/by-nc/4.0/
Data and Python scripts are free to use and distribute